variable "name" {
  type = string
  default = "default-filter"
}

variable "tenant" {
  type = string                                
  default = "default-tenant"
}

variable "filter_attr" {
  type = list                                
  default = []                    
}

