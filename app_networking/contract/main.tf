resource "aci_contract" "this" {
  tenant_dn = var.tenant
  name      = var.name
}

resource "aci_contract_subject" "this" {
  contract_dn                  = aci_contract.this.id
  name                         = "${var.name}-Subject"
  relation_vz_rs_subj_filt_att = var.filter_attr
}
