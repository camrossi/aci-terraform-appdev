# Using a single workspace:
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "camrossi"

    workspaces {
      name = "aci-terraform-appdev"
    }
  }
}
