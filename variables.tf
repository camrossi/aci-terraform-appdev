variable "aci_username" {
  type = string
}
variable "aci_password" {
  type = string
}
variable "apic_url" {
  type = string
}
variable "aws_access_key" {
  type = string
}
variable "aws_secret_key" {
  type = string
}
variable "app" {
  type = string
}
variable "tiers" {
  type = list
  default = []
}
#variable "priv_key" {
#  type = string 
#} 
variable "ami_image" {
  type = string
}
variable "instance_type" {
  type = string
}
#variable "instance_key" {
#  type = string
#}
variable "match_expression" {
  type = list
  default =[]
}
variable "priv_key_1" {
  type = string 
}
variable "instance_key_1" {
  type = string 
}
variable "allow_policy" {
  type = list
  default = []
}
variable "inet_access" {
  type = string      
} 
